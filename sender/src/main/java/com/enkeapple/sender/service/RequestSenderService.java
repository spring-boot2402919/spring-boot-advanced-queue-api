package com.enkeapple.sender.service;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.retry.annotation.Recover;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.UUID;
import java.util.Map;

@Service
@Slf4j
public class RequestSenderService {
    private final RestTemplate restTemplate;
    private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);
    private final String serverUrl;

    @Autowired
    public RequestSenderService(@Value("${application.url}") String serverUrl, RestTemplate restTemplate) {
        this.serverUrl = serverUrl;
        this.restTemplate = restTemplate;
    }

    @Scheduled(fixedRate = 1000)
    public void sendRequests() {
        for (int i = 0; i < 100; i++) {
            executorService.submit(this::sendRequest);
        }
    }

    @Retry(name = "sendRequest")
    @RateLimiter(name = "sendRequest")
    public void sendRequest() {
        String data = generateData();
        String idempotencyKey = UUID.randomUUID().toString();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Idempotency-Key", idempotencyKey);

        HttpEntity<String> requestEntity = new HttpEntity<>(data, headers);

        try {
            restTemplate.postForObject(serverUrl, requestEntity, String.class);

            log.info("Request sent: " + data + " with Idempotency-Key: " + idempotencyKey);
        } catch (RestClientException e) {
            log.error("Request failed: " + data + " with Idempotency-Key: " + idempotencyKey, e);

            throw e;
        }
    }

    @Recover
    public void fallback(RestClientException e) {
        log.error("Request failed after retries or rate limit exceeded: " + e.getMessage(), e);
    }

    private String generateData() {
        return "Data-" + ThreadLocalRandom.current().nextInt(1000);
    }
}
