package com.enkeapple.receiver.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class RequestReceiverService {
    private final BlockingQueue<String> requestQueue = new LinkedBlockingQueue<>();
    private final AtomicInteger requestCount = new AtomicInteger(0);
    private final int rateLimit;
    private final Set<String> processedRequests = new HashSet<>();

    public RequestReceiverService(@Value("${application.rateLimit}") int rateLimit) {
        this.rateLimit = rateLimit;
    }

    public ResponseEntity<String> processRequest(String data, String idempotencyKey) {
        synchronized (processedRequests) {
            if (processedRequests.contains(idempotencyKey)) {
                log.info("Duplicate request detected: " + idempotencyKey);

                return new ResponseEntity<>("Duplicate request", HttpStatus.OK);
            }
            processedRequests.add(idempotencyKey);
        }

        if (requestCount.incrementAndGet() > rateLimit) {
            log.warn("Rate limit exceeded for data: " + data);

            return new ResponseEntity<>("Too many requests: please slow down", HttpStatus.TOO_MANY_REQUESTS);
        }

        requestQueue.offer(data);

        log.info("Request accepted: " + data);

        return new ResponseEntity<>("Request accepted", HttpStatus.OK);
    }

    @Scheduled(fixedRate = 100)
    public void processQueue() {
        for (int i = 0; i < 10; i++) {
            String data = requestQueue.poll();

            if (data != null) {
                process(data);
            }
        }

        requestCount.set(0);
    }

    private void process(String data) {
        log.info("Processing: " + data);
    }
}
