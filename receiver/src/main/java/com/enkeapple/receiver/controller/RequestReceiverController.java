package com.enkeapple.receiver.controller;

import com.enkeapple.receiver.service.RequestReceiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RequestReceiverController {
    private final RequestReceiverService requestReceiverService;

    @Autowired
    public RequestReceiverController(RequestReceiverService requestReceiverService) {
        this.requestReceiverService = requestReceiverService;
    }

    @PostMapping("/process")
    @Cacheable(value = "requests", key = "#idempotencyKey")
    public ResponseEntity<String> processRequest(@RequestBody String data, @RequestHeader("Idempotency-Key") String idempotencyKey) {
        return requestReceiverService.processRequest(data, idempotencyKey);
    }

}
